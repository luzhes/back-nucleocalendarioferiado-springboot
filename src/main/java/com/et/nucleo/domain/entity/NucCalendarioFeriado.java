package com.et.nucleo.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.math.BigInteger;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "nuc_calendario_feriados")
public class NucCalendarioFeriado extends Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Integer anio;
  private Integer mes;
  private Integer dia;
  private String descripcion;
}

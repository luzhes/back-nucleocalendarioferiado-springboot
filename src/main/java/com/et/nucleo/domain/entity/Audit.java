package com.et.nucleo.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@Getter
@Setter
@MappedSuperclass

public class Audit {

  @Column(updatable = false, name = "fecha_registro")
  @CreationTimestamp
  @JsonIgnore
  public LocalDateTime fechaRegistro;
  @Column(updatable = false, name = "fecha_actualizacion")
  @UpdateTimestamp
  @JsonIgnore
  public LocalDateTime fechaActualizacion;
  @Column(updatable = false, name = "usuario_registro")
  @JsonIgnore
  public String usuarioRegistro;
  @Column(name = "usuario_actualizacion")
  @JsonIgnore
  public String usuarioActualizacion;
}

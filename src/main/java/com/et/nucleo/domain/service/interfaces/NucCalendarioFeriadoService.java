package com.et.nucleo.domain.service.interfaces;

import com.et.nucleo.domain.entity.NucCalendarioFeriado;
import com.et.nucleo.presentation.controller.request.dto.NucCalendarioFeriadoDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
public interface NucCalendarioFeriadoService extends CRUDService<NucCalendarioFeriado, Long> {
  NucCalendarioFeriado createHoliday(NucCalendarioFeriadoDto calendarioFeriadoDto);
  NucCalendarioFeriado updateHoliday(Long id, NucCalendarioFeriadoDto calendarioFeriadoDto);
  NucCalendarioFeriado updatePatch(Long id, JsonPatch patch)
      throws JsonPatchException, JsonProcessingException;
}


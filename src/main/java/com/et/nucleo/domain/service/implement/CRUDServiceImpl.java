package com.et.nucleo.domain.service.implement;

import com.et.nucleo.data.repository.GenericRepository;
import com.et.nucleo.domain.service.interfaces.CRUDService;
import com.et.nucleo.exception.EntityNotFoundException;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
public abstract class CRUDServiceImpl<T, ID> implements CRUDService<T, ID> {

  protected abstract GenericRepository<T, ID> getRepository();

  @Override
  public T create(T t) {
    return getRepository ( ).save (t);
  }

  @Override
  public T update(ID id, T t) {
    getRepository ( ).findById (id)
        .orElseThrow (() -> new EntityNotFoundException ("ID NOT FOUND " + id));
    return getRepository ( ).save (t);
  }

  @Override
  public List<T> getAll() {
    return getRepository ( ).findAll ( );
  }

  @Override
  public List<T> getAllSort(Sort sort) {
    return getRepository ( ).findAll (sort);
  }

  @Override
  public T getById(ID id) {
    return getRepository ( ).findById (id)
        .orElseThrow (() -> new EntityNotFoundException ("ID NOT FOUND: " + id));
  }

  @Override
  public void delete(ID id) {
    getRepository ( ).findById (id)
        .orElseThrow (() -> new EntityNotFoundException ("ID NOT FOUND " + id));
    getRepository ( ).deleteById (id);
  }
}

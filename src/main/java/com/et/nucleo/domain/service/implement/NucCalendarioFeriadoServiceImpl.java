package com.et.nucleo.domain.service.implement;

import com.et.nucleo.data.repository.GenericRepository;
import com.et.nucleo.data.repository.NucCalendarioFeriadoRepository;
import com.et.nucleo.domain.entity.NucCalendarioFeriado;
import com.et.nucleo.domain.mapper.NucCalendarioFeriadoMapper;
import com.et.nucleo.domain.service.interfaces.NucCalendarioFeriadoService;
import com.et.nucleo.exception.DuplicateHolidayException;
import com.et.nucleo.exception.EntityNotFoundException;
import com.et.nucleo.presentation.controller.request.dto.NucCalendarioFeriadoDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import jakarta.transaction.Transactional;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@AllArgsConstructor
@Service
public class NucCalendarioFeriadoServiceImpl extends
    CRUDServiceImpl<NucCalendarioFeriado, Long> implements
    NucCalendarioFeriadoService {

  private NucCalendarioFeriadoRepository calendarioFeriadoRepository;
  private NucCalendarioFeriadoMapper calendarioFeriadoMapper;

  @Override
  protected GenericRepository<NucCalendarioFeriado, Long> getRepository() {
    return calendarioFeriadoRepository;
  }

  @Transactional
  public NucCalendarioFeriado createHoliday(NucCalendarioFeriadoDto calendarioFeriadoDto) {
    // Verificamos si ya existe un feriado con la misma fecha
    Optional<NucCalendarioFeriado> existingHoliday = calendarioFeriadoRepository.findByAnioAndMesAndDia (
        calendarioFeriadoDto.getAnio ( ), calendarioFeriadoDto.getMes ( ),
        calendarioFeriadoDto.getDia ( ));

    if (existingHoliday.isPresent ( )) {
      throw new DuplicateHolidayException ("El feriado para la fecha especificada ya existe.");
    }

    NucCalendarioFeriado calendarioFeriado = calendarioFeriadoMapper.fromDtoToEntity (null,
        calendarioFeriadoDto);
    return calendarioFeriadoRepository.save (calendarioFeriado);
  }

  @Override
  public NucCalendarioFeriado updateHoliday(Long id, NucCalendarioFeriadoDto calendarioFeriadoDto) {
    NucCalendarioFeriado calendarioFeriadoFound = calendarioFeriadoRepository.findById (id)
        .orElseThrow (() -> new EntityNotFoundException ("Calendario Feriado no existe"));
    NucCalendarioFeriado calendarioFeriado = calendarioFeriadoMapper.fromDtoToEntity (
        calendarioFeriadoFound,
        calendarioFeriadoDto);
    return calendarioFeriadoRepository.save (calendarioFeriado);
  }

  @Override
  public NucCalendarioFeriado updatePatch(Long id, JsonPatch patch)
      throws JsonPatchException, JsonProcessingException {
    return null;
  }
}

package com.et.nucleo.domain.service.interfaces;

import java.util.List;
import org.springframework.data.domain.Sort;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
public interface CRUDService<T, ID> {

  T create(T t);

  T update(ID id, T t);

  List<T> getAll();

  List<T> getAllSort(Sort sort);

  T getById(ID id);

  void delete(ID id);
}

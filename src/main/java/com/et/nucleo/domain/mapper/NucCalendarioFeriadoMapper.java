package com.et.nucleo.domain.mapper;

import com.et.nucleo.domain.entity.NucCalendarioFeriado;
import com.et.nucleo.presentation.controller.request.dto.NucCalendarioFeriadoDto;
import org.springframework.stereotype.Component;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@Component
public class NucCalendarioFeriadoMapper {

  public NucCalendarioFeriado fromDtoToEntity(NucCalendarioFeriado calendarioFeriadoFound,
      NucCalendarioFeriadoDto dto) {
    NucCalendarioFeriado calendarioFeriado = new NucCalendarioFeriado ( );
    if (calendarioFeriadoFound != null) {
      calendarioFeriado = calendarioFeriadoFound;
    }
    calendarioFeriado.setAnio (Math.toIntExact (dto.getAnio ( )));
    calendarioFeriado.setMes (Math.toIntExact (dto.getMes ( )));
    calendarioFeriado.setDia (Math.toIntExact (dto.getDia ( )));
    calendarioFeriado.setDescripcion (dto.getDescripcion ( ));
    return calendarioFeriado;
  }

}

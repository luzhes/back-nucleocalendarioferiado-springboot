package com.et.nucleo.presentation.controller;

import com.et.nucleo.domain.entity.NucCalendarioFeriado;
import com.et.nucleo.domain.service.interfaces.NucCalendarioFeriadoService;
import com.et.nucleo.presentation.controller.request.dto.NucCalendarioFeriadoDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@Slf4j
@AllArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/nuc_calendario_feriado")
@Tag(name = "Calendario Feriado", description = "Controlador de servicios para la tabla 'nuc_calendario_feriado'")

public class NucCalendarioFeriadoController {

  private NucCalendarioFeriadoService calendarioFeriadoService;
  Gson prettyGson = new GsonBuilder ( ).setPrettyPrinting ( ).create ( );

  @Operation(
      summary = "Recuperar la lista de 'Calendario de Feriados' registrados",
      description = "Obtenga la lista completa de 'Calendario de Feriados'. El servicio devuelve un array de objetos 'Feriados del Calendario'.",
      responses = {
          @ApiResponse(responseCode = "200", content = {
              @Content(schema = @Schema(implementation = NucCalendarioFeriado.class), mediaType = "application/json")
          }),
          @ApiResponse(responseCode = "404", content = {
              @Content(schema = @Schema())
          }),
          @ApiResponse(responseCode = "500", content = {
              @Content(schema = @Schema())
          })
      }
  )

  @GetMapping
  public ResponseEntity<List<NucCalendarioFeriado>> getAll() {
    log.info ("<--------------------------- GET CALENDARIO FERIADO -------------------------> \n");
    List<NucCalendarioFeriado> calendarioFeriados = calendarioFeriadoService.getAll ( );
    return ResponseEntity.status (HttpStatus.OK).body (calendarioFeriados);
  }


  @Operation(
      summary = "Recuperar por 'ID' del 'Calendario Feriado' registrado",
      description = "Obtenga un objeto 'Calendario Feriado' filtrado por 'ID'",
      responses = {
          @ApiResponse(responseCode = "200", content = {
              @Content(schema = @Schema(implementation = NucCalendarioFeriado.class), mediaType = "application/json")
          }),
          @ApiResponse(responseCode = "404", content = {
              @Content(schema = @Schema())
          }),
          @ApiResponse(responseCode = "500", content = {
              @Content(schema = @Schema())
          })
      }
  )

  @GetMapping("/{id}")
  public ResponseEntity<NucCalendarioFeriado> getById(@PathVariable Long id) {
    NucCalendarioFeriado calendarioFeriadoFoundById = calendarioFeriadoService.getById (id);
    log.info ("<---------------- GEY BY ID CALENDARIO FERIADO ---------------------> \n"
        + prettyGson.toJson (id));
    return ResponseEntity.status (HttpStatus.OK).body (calendarioFeriadoFoundById);
  }

  @Operation(
      summary = "Crea un registro para un Calendario Feriado en la tabla 'nuc_calendario_feriado'",
      description = "Crea un objeto 'NucCalendarioFeriado'. El servicio devuelve un objeto 'NucCalendarioFeriado' ",
      responses = {@ApiResponse(responseCode = "200", content = {
          @Content(schema = @Schema(implementation = NucCalendarioFeriado.class), mediaType = "application/json")
      }),
          @ApiResponse(responseCode = "400", content = {
              @Content(mediaType = "plain/text",
                  schema = @Schema(
                      implementation = ErrorResponse.class
                  ))
          }),
          @ApiResponse(responseCode = "404", content = {
              @Content(mediaType = "plain/text",
                  schema = @Schema(
                      implementation = ErrorResponse.class
                  ))
          }),
          @ApiResponse(responseCode = "500", content = {
              @Content(mediaType = "plain/text",
                  schema = @Schema(
                      implementation = ErrorResponse.class
                  ))
          })})
  @PostMapping()
  public ResponseEntity<NucCalendarioFeriado> save(
      @Valid @RequestBody NucCalendarioFeriadoDto calendarioFeriadoDto) {
    log.info (
        "<----------------- SAVE CALENDARIO FERIADO -------------------> \n" + prettyGson.toJson (
            calendarioFeriadoDto));
    return ResponseEntity.status (HttpStatus.CREATED)
        .body (calendarioFeriadoService.createHoliday (calendarioFeriadoDto));
  }

  @Operation(
      summary = "Actualiza un registro de la tabla 'nuc_calendario_feriado'",
      description = "Actualiza un objeto 'NucCalendarioFeriado'. El servicio devuelve el objeto 'NucCalendarioFeriado' actualizado ",

      responses = {@ApiResponse(responseCode = "200", content = {
          @Content(schema = @Schema(implementation = NucCalendarioFeriado.class), mediaType = "application/json")
      }),
          @ApiResponse(responseCode = "404", content = {
              @Content(schema = @Schema())
          }),
          @ApiResponse(responseCode = "500", content = {
              @Content(schema = @Schema())
          })})
  @PutMapping("/{id}")
  public ResponseEntity<NucCalendarioFeriado> update(@Valid @PathVariable Long id,
      @RequestBody NucCalendarioFeriadoDto calendarioFeriadoDto) {
    log.info (
        "<----------------- UPDATE SENDER -----------------> \n" + prettyGson.toJson (
            calendarioFeriadoDto));

    NucCalendarioFeriado calendarioFeriadoUpdated = calendarioFeriadoService.updateHoliday (id,
        calendarioFeriadoDto);
    return ResponseEntity.status (HttpStatus.OK).body (calendarioFeriadoUpdated);
  }

  @Operation(
      summary = "Eliminar un registro de 'NucCalendarioFeriado'",
      description = "Elimina un registro de la tabla 'nuc_calendario_feriado'. No se devuelve ningun objeto, pero si un status http 204",

      responses = {@ApiResponse(responseCode = "204", content = {
          @Content(schema = @Schema(implementation = NucCalendarioFeriado.class), mediaType = "application/json")
      }),
          @ApiResponse(responseCode = "404", content = {
              @Content(schema = @Schema())
          }),
          @ApiResponse(responseCode = "500", content = {
              @Content(schema = @Schema())
          })})
  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable Long id) {
    log.info ("<---------------- DELETE SENDER --------------------> \n" + prettyGson.toJson (id));

    calendarioFeriadoService.delete (id);
    return ResponseEntity.ok ( ).build ( );
  }

  /*
  @PatchMapping("/{id}")
  public ResponseEntity<NucCalendarioFeriado> updatePatch(@PathVariable Long id,
      @RequestBody JsonPatch patch)
      throws JsonPatchException, JsonProcessingException {
    NucCalendarioFeriado calendarioFeriadoPatch = calendarioFeriadoService.updatePatch (id, patch);
    return ResponseEntity.status (HttpStatus.OK).body (calendarioFeriadoPatch);
  }

   */
}

package com.et.nucleo.presentation.controller.request.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@NoArgsConstructor
@Getter
@Setter

public class NucCalendarioFeriadoDto {

  @NotNull(message = "{calendario-feriado.anio.not-null}")
  private int anio;
  @NotNull(message = "{calendario-feriado.mes.not-null}")
  @Min(value = 1, message = "{calendario-feriado.mes.min}")
  @Max(value = 12, message = "{calendario-feriado.mes.max}")
  private int mes;

  @NotNull(message = "{calendario-feriado.dia.not-null}")
  @Min(value = 1, message = "{calendario-feriado.dia.min}")
  @Max(value = 31, message = "{calendario-feriado.dia.max}")
  private int dia;

  @NotBlank(message = "{calendario-feriado.descripcion.not-blank}")
  @Size(min = 3, max = 200, message = "{calendario-feriado.descripcion.size}")
  private String descripcion;
}

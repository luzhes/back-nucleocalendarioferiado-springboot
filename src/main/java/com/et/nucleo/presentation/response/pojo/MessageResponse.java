package com.et.nucleo.presentation.response.pojo;

import com.et.nucleo.common.Pojo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Luz Heidy Condori Loza on 03-06-2024.
 */

@Pojo
@Getter
@Setter
@NoArgsConstructor
public class MessageResponse {

  private String message;

  public MessageResponse(String message) {
    this.message = message;
  }
}

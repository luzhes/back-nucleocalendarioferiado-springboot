package com.et.nucleo.exception;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
public class EntityNotFoundException extends RuntimeException{
  public EntityNotFoundException(String message) {
    super(message);
  }

  public EntityNotFoundException(String entity, Integer id) {
    super(String.format("%s con id %s no encontrado", entity, id.toString()));
  }

  //utilizada solo para la entidad App
  public EntityNotFoundException(String entity, String code){
    super(String.format("%s with appCode: %s , was not found", entity, code));
  }

}

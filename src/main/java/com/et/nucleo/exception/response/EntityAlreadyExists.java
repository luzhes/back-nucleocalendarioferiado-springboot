package com.et.nucleo.exception.response;

public class EntityAlreadyExists extends RuntimeException{
  private static final String ERROR_MESSAGE = "%s %s already exists";
  public EntityAlreadyExists(String entity, String name){
    super(String.format(ERROR_MESSAGE,entity,name));

  }
}

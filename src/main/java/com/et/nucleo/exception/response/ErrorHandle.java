package com.et.nucleo.exception.response;

import com.et.nucleo.exception.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Luz Heidy Condori Loza on 03-06-2024.
 */
public class ErrorHandle {
  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorResponse> handlerEntityNotFoundException(HttpServletRequest req,
      Exception ex) {
    HttpStatus status = HttpStatus.NOT_FOUND;
    ErrorResponse response = new ErrorResponse(status.value(), status.name(), ex.getMessage(),
        req.getRequestURI());
    return ResponseEntity.status(status).body(response);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ValidationErrorResponse> handleValidException(
      MethodArgumentNotValidException ex) {
    List<FieldErrorModel> errors = ex.getBindingResult ( ).getAllErrors ( ).stream ( )
        .map (fieldError -> {
          FieldErrorModel fieldErrorModel = new FieldErrorModel ( );
          fieldErrorModel.setCode (fieldError.getCode ( ));
          fieldErrorModel.setMessage (fieldError.getDefaultMessage ( ));
          fieldErrorModel.setField (((FieldError) fieldError).getField ( ));
          return fieldErrorModel;
        }).toList ( );
    HttpStatus status = HttpStatus.BAD_REQUEST;
    ValidationErrorResponse response = new ValidationErrorResponse ( );
    response.setCode (status.value ( ));
    response.setErrors (errors);
    return ResponseEntity.status (status).body (response);
  }

}

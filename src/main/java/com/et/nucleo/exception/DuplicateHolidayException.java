package com.et.nucleo.exception;

/**
 * @author Luz Heidy Condori Loza on 03-06-2024.
 */
public class DuplicateHolidayException extends RuntimeException {
  public DuplicateHolidayException(String message) {
    super(message);
  }

}

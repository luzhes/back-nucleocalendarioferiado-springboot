package com.et.nucleo.data.repository;

import com.et.nucleo.domain.entity.NucCalendarioFeriado;
import java.util.Optional;
import org.springframework.stereotype.Repository;

/**
 * @author Luz Heidy Condori Loza on 31-05-2024.
 */
@Repository
public interface NucCalendarioFeriadoRepository extends
    GenericRepository<NucCalendarioFeriado, Long> {
  Optional<NucCalendarioFeriado> findByAnioAndMesAndDia(Integer anio, Integer mes, Integer dia);

}

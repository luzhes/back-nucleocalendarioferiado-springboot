package com.et.nucleo.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

/**
 * @author Luz Heidy Condori Loza on 03-06-2024.
 */
@OpenAPIDefinition(
    info = @Info(
        title = "Store API",
        description = "Store API Example",
        version = "1.0",
        contact = @Contact(
            name = "Heidy Condori Loza",
            email = "luzheidyc.loza@gmail.com",
            url = "https://bo.linkedin.com/in/luz-condori-loza-957a95272"
        )
    )
)
public class SwaggerConfig {

}
